import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    df['Title'] = df['Name'].str.extract(r', (.*?)\.')

    result = []

    for title in ["Mr", "Mrs", "Miss"]:
        data = (df['Title'] == title)
        missing_values = df[data]['Age'].isnull().sum()

        median_age = df[data]['Age'].median()
        if not pd.isnull(median_age):
            median_age = round(median_age)

        title_answer = title + "."

        result.append((title_answer, missing_values, median_age))

    return result 